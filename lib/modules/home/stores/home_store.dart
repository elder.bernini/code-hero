import 'package:code_hero/core/datasource/http/api_datasource_impl.dart';
import 'package:code_hero/core/datasource/http/dio/dio_client.dart';
import 'package:code_hero/core/helpers/states/state_result.dart';
import 'package:code_hero/core/logger/logger.dart';
import 'package:code_hero/modules/home/models/paginator.dart';
import 'package:code_hero/modules/home/services/home_service.dart';
import 'package:code_hero/modules/home/states/home_states.dart';
import 'package:mobx/mobx.dart';

part 'home_store.g.dart';

class HomeStore = HomeStoreBase with _$HomeStore;

abstract class HomeStoreBase with Store {
  Paginator paginator;
  String? nameSearch;

  final HomeService _homeService;

  HomeStoreBase()
      : _homeService = HomeService(
            apiDatasource:
                ApiDatasourceImpl(dioClient: getDio(), logger: LoggerImpl())),
        paginator = Paginator(
          currentPage: 1,
        );

  @observable
  StateResult state = DefaultStateResult();

  @observable
  StateResult stateLoading = DefaultStateResult();

  @action
  void setState(StateResult value) => state = value;

  @action
  void setStateLoading(StateResult value) => stateLoading = value;

  void fetchSearch(String? searchValue) {
    nameSearch = searchValue;
    fetchPage(Paginator.fromPage(1));
  }

  Future<void> fetchPage(Paginator page) async {
    setStateLoading(LoadingStateResult());

    var res = await _homeService.get(
      page.amoutPerPage,
      page.pageOffset,
      nameSearch,
    );

    var st = res.fold(
      (l) => FailureStateResult(l),
      (resPage) {
        if (resPage.results.isEmpty) {
          return HomeSearchEmptyStateResult();
        }

        paginator = Paginator(
          totalItems: resPage.total,
          currentPage: page.currentPage,
        );

        return SuccessStateResult(resPage.results);
      },
    );

    setStateLoading(HideLoadingStateResult());
    setState(st);
  }
}
