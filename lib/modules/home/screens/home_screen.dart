import 'package:code_hero/core/design/colors/app_colors.dart';
import 'package:code_hero/core/design/typography/text_style.dart';
import 'package:code_hero/core/helpers/failure/failure.dart';
import 'package:code_hero/core/helpers/states/state_result.dart';
import 'package:code_hero/core/helpers/utils.dart';
import 'package:code_hero/modules/home/models/character.dart';
import 'package:code_hero/modules/home/screens/components/home_character_list.dart';
import 'package:code_hero/modules/home/screens/components/home_paginator.dart';
import 'package:code_hero/modules/home/screens/components/home_top.dart';
import 'package:code_hero/modules/home/states/home_states.dart';
import 'package:code_hero/modules/home/stores/home_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

class HomeScrren extends StatefulWidget {
  const HomeScrren({super.key});

  @override
  State<HomeScrren> createState() => _HomeScrrenState();
}

class _HomeScrrenState extends State<HomeScrren> {
  final _homeStore = HomeStore();

  ReactionDisposer? _disposerState;

  @override
  void initState() {
    super.initState();

    _disposerState =
        reaction<StateResult>((_) => _homeStore.stateLoading, (state) {
      checkLoading(context, state);
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _homeStore.fetchPage(_homeStore.paginator);
    });
  }

  @override
  void dispose() {
    super.dispose();

    _disposerState?.call();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            HomeTop(
              onSearch: _homeStore.fetchSearch,
            ),
            Container(
              decoration: const BoxDecoration(color: primary),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 12, horizontal: 112),
                child: Text(
                  'Nome',
                  style: pRegular.copyWith(color: Colors.white),
                ),
              ),
            ),
            Observer(builder: (c) {
              var state = _homeStore.state;

              if (state is HomeSearchEmptyStateResult) {
                return _stateEmpty();
              }
              if (state is SuccessStateResult<List<Character>>) {
                return _stateSuccess(state.data);
              }
              if (state is FailureStateResult) {
                return _stateError(state.failure);
              }
              return const SizedBox.shrink();
            }),
          ],
        ),
      ),
    );
  }

  Widget _stateSuccess(List<Character> items) {
    return Expanded(
      child: Column(
        children: [
          Expanded(
            child: HomecharacterList(
              characters: items,
              onTapItem: (itemId) => debugPrint('tap: $itemId'),
            ),
          ),
          HomePaginator(
            paginator: _homeStore.paginator,
            onFetchPage: _homeStore.fetchPage,
          ),
        ],
      ),
    );
  }

  Widget _stateEmpty() {
    return const Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Nenhum resultado encontrado com o filtro informado.',
            style: p2Regular,
            textAlign: TextAlign.center,
          ),
          Text(
            'O nome deve corresponder exatamente ao nome do herói.',
            textAlign: TextAlign.center,
            style: pRegular,
          ),
        ],
      ),
    );
  }

  Widget _stateError(Failure failure) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'Oops..',
            style: p2Regular,
          ),
          Text(
            failure.message,
            style: pRegular,
          ),
          const SizedBox(
            height: 24,
          ),
          FilledButton(
            onPressed: () => _homeStore.fetchPage(_homeStore.paginator),
            child: const Text('Tentar novamente'),
          ),
        ],
      ),
    );
  }
}
