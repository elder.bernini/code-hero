import 'package:code_hero/core/design/colors/app_colors.dart';
import 'package:code_hero/core/design/typography/text_style.dart';
import 'package:code_hero/core/helpers/type_defs/function_type_defs.dart';
import 'package:flutter/material.dart';

class HomeTop extends StatelessWidget {
  final VoidTypedCallback<String> onSearch;

  const HomeTop({
    super.key,
    required this.onSearch,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.symmetric(
        vertical: 12,
        horizontal: 32,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration: const BoxDecoration(
                  border: Border(
                    bottom: BorderSide(color: primary, width: 2),
                  ),
                ),
                child: const Text(
                  'BUSCA',
                  style: titleBlack,
                ),
              ),
              const Text(
                ' MARVEL',
                style: titleBlack,
              ),
              const Text('TESTE MOBILE', style: titleLight),
            ],
          ),
          const SizedBox(height: 12),
          Text(
            'Nome do personagem',
            style: pRegular.copyWith(color: primary),
          ),
          TextFormField(
            decoration: const InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
            ),
            textInputAction: TextInputAction.search,
            onFieldSubmitted: _search,
          ),
        ],
      ),
    );
  }

  void _search(String? value) {
    if (value != null) {
      onSearch(value);
    }
  }
}
