import 'package:code_hero/core/design/typography/text_style.dart';
import 'package:code_hero/core/helpers/type_defs/function_type_defs.dart';
import 'package:code_hero/modules/home/models/character.dart';
import 'package:flutter/material.dart';

class HomeCharacterListItem extends StatelessWidget {
  final Character character;
  final VoidTypedCallback<int> onTapItem;

  const HomeCharacterListItem({
    super.key,
    required this.character,
    required this.onTapItem,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTapItem(character.id),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 16,
          horizontal: 24,
        ),
        child: Row(
          children: [
            CircleAvatar(
              radius: 30,
              backgroundImage: NetworkImage(
                character.thumbnail.fullPath,
              ),
            ),
            const SizedBox(
              width: 24,
            ),
            Expanded(
              child: Text(
                character.name,
                style: p2Regular,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
