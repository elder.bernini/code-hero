import 'package:code_hero/core/design/colors/app_colors.dart';
import 'package:code_hero/core/helpers/type_defs/function_type_defs.dart';
import 'package:code_hero/modules/home/models/character.dart';
import 'package:code_hero/modules/home/screens/components/home_character_list_item.dart';
import 'package:flutter/material.dart';

class HomecharacterList extends StatelessWidget {
  final List<Character> characters;
  final VoidTypedCallback<int> onTapItem;

  const HomecharacterList({
    super.key,
    required this.characters,
    required this.onTapItem,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4),
      child: ListView.separated(
        shrinkWrap: true,
        itemBuilder: (context, index) => HomeCharacterListItem(
          character: characters[index],
          onTapItem: onTapItem,
        ),
        separatorBuilder: (_, i) => const Divider(
          height: 1,
          color: primary,
        ),
        itemCount: characters.length,
      ),
    );
  }
}
