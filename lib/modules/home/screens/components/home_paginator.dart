import 'package:code_hero/core/design/colors/app_colors.dart';
import 'package:code_hero/core/design/typography/text_style.dart';
import 'package:code_hero/core/helpers/type_defs/function_type_defs.dart';
import 'package:code_hero/modules/home/models/paginator.dart';
import 'package:flutter/material.dart';

class HomePaginator extends StatelessWidget {
  final Paginator paginator;
  final VoidTypedCallback<Paginator> onFetchPage;

  const HomePaginator({
    super.key,
    required this.paginator,
    required this.onFetchPage,
  });

  @override
  Widget build(BuildContext context) {
    if (paginator.hasPages) {
      return const SizedBox.shrink();
    }
    return Padding(
      padding: const EdgeInsets.only(bottom: 24, top: 18, left: 20, right: 20),
      child: Row(
        children: [
          IconButton(
            onPressed: paginator.hasPreviusPage ? _fetchPreviusPage : null,
            icon: const Icon(
              size: 50,
              Icons.arrow_left_sharp,
              color: primary,
            ),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: _pageNumbers(),
            ),
          ),
          IconButton(
            onPressed: paginator.hasNextPage ? _nextPage : null,
            icon: const Icon(
              Icons.arrow_right_sharp,
              size: 50,
              color: primary,
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> _pageNumbers() {
    return paginator.pageNumbers
        .map(
          (page) => FilledButton(
            onPressed: () => _fetchPage(page),
            style: _buttonStyle(page),
            child: Text(
              page.toString(),
              style: p2Regular.copyWith(
                  color:
                      paginator.isCurrentPage(page) ? Colors.white : primary),
            ),
          ),
        )
        .toList();
  }

  ButtonStyle _buttonStyle(int page) {
    if (paginator.isCurrentPage(page)) {
      return FilledButton.styleFrom(
        shape: const CircleBorder(),
      );
    }
    return FilledButton.styleFrom(
      shape: const CircleBorder(
        side: BorderSide(color: primary, width: 2),
      ),
      backgroundColor: Colors.white,
      foregroundColor: primary,
    );
  }

  void _fetchPreviusPage() {
    onFetchPage(paginator.previusPage());
  }

  void _nextPage() {
    onFetchPage(paginator.nextPage());
  }

  void _fetchPage(int page) {
    if (paginator.isCurrentPage(page)) {
      return;
    }
    onFetchPage(Paginator.fromPage(page));
  }
}
