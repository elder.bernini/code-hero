import 'package:code_hero/core/datasource/http/api_datasource.dart';
import 'package:code_hero/core/helpers/failure/failure.dart';
import 'package:code_hero/modules/home/models/page_character.dart';
import 'package:dartz/dartz.dart';

class HomeService {
  final ApiDatasource _apiDatasource;

  HomeService({
    required ApiDatasource apiDatasource,
  }) : _apiDatasource = apiDatasource;

  Future<Either<Failure, PageCharacter>> get(
    int limit,
    int offset,
    String? name,
  ) {
    Map<String, dynamic> params = {
      'limit': limit,
      'offset': offset,
    };

    if (name?.isNotEmpty == true) {
      params.addAll({
        'name': name,
      });
    }

    return _apiDatasource.get(
      '/characters',
      PageCharacter.fromJson,
      params: params,
    );
  }
}
