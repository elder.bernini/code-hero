import 'package:code_hero/modules/home/models/character.dart';
import 'package:json_annotation/json_annotation.dart';

part 'page_character.g.dart';

@JsonSerializable(createToJson: false)
class PageCharacter {
  final int limit;
  final int total;
  final int count;
  final List<Character> results;

  const PageCharacter({
    required this.limit,
    required this.total,
    required this.count,
    required this.results,
  });

  factory PageCharacter.fromJson(dynamic json) => _$PageCharacterFromJson(json);
}
