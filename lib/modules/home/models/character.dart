import 'package:code_hero/modules/home/models/thumbnail_character.dart';
import 'package:json_annotation/json_annotation.dart';

part 'character.g.dart';

@JsonSerializable(createToJson: false)
class Character {
  final int id;
  final String name;
  final ThumbnailCharacter thumbnail;

  const Character(
      {required this.id, required this.name, required this.thumbnail});

  factory Character.fromJson(dynamic json) => _$CharacterFromJson(json);
}
