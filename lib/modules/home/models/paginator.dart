class Paginator {
  final int currentPage;
  final int totalItems;
  final int amoutPerPage;
  final int firstPage = 1;
  final int pagesToShow = 2;
  final int totalPages;

  Paginator({
    required this.currentPage,
    this.totalItems = 0,
    this.amoutPerPage = 4,
  }) : totalPages = (totalItems / amoutPerPage).ceil();

  bool get hasPages => totalPages <= 1;
  bool get hasPreviusPage => currentPage > 1;
  bool get hasNextPage => currentPage < totalPages;

  List<int> get pageNumbers {
    if ((currentPage - firstPage) < pagesToShow) {
      return [firstPage, firstPage + 1, firstPage + 2];
    } else if ((totalPages - currentPage) < pagesToShow) {
      return [totalPages - 2, totalPages - 1, totalPages];
    } else {
      return [currentPage - 1, currentPage, currentPage + 1];
    }
  }

  bool isCurrentPage(int page) => page == currentPage;
  int get pageOffset => (currentPage - 1) * amoutPerPage;

  Paginator previusPage() {
    return Paginator(
      currentPage: currentPage - 1,
      totalItems: totalItems,
    );
  }

  Paginator nextPage() {
    return Paginator(
      currentPage: currentPage + 1,
      totalItems: totalItems,
    );
  }

  factory Paginator.fromPage(int page) => Paginator(currentPage: page);
}
