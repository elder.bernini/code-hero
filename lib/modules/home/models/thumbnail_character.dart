import 'package:json_annotation/json_annotation.dart';

part 'thumbnail_character.g.dart';

@JsonSerializable(createToJson: false)
class ThumbnailCharacter {
  final String path;

  @JsonKey(name: 'extension')
  final String fileExtension;

  const ThumbnailCharacter({
    required this.path,
    required this.fileExtension,
  });

  factory ThumbnailCharacter.fromJson(dynamic json) =>
      _$ThumbnailCharacterFromJson(json);

  String get fullPath => '$path.$fileExtension';
}
