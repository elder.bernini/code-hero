import 'package:code_hero/core/design/colors/app_colors.dart';
import 'package:code_hero/core/design/theme/input_theme.dart';
import 'package:code_hero/modules/home/screens/home_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Code Hero',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: primary),
        useMaterial3: true,
        inputDecorationTheme: inputDecorationTheme,
      ),
      home: const HomeScrren(),
    );
  }
}
