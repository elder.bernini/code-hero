import 'package:code_hero/core/helpers/failure/failure.dart';
import 'package:code_hero/core/helpers/type_defs/function_type_defs.dart';
import 'package:dartz/dartz.dart';

abstract class ApiDatasource {
  Future<Either<Failure, T>> get<T>(
    String path,
    FromJsonFactory<T> responseFactory, {
    Map<String, dynamic>? params,
  });
}
