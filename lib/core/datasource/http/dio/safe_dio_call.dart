import 'package:dio/dio.dart';

abstract class SafeDioCall {
  final Dio _dio;
  final String path;
  final Map<String, dynamic>? params;

  SafeDioCall(
    this._dio,
    String path,
    this.params,
  ) : path = path.startsWith('/') ? path : '/$path';

  Future<Response<dynamic>> execute();
}

class GetSafeDioCall extends SafeDioCall {
  GetSafeDioCall({
    required Dio dio,
    required String path,
    Map<String, dynamic>? params,
  }) : super(
          dio,
          path,
          params,
        );

  @override
  Future<Response<dynamic>> execute() {
    return _dio.get(
      path,
      queryParameters: params,
    );
  }
}
