import 'package:code_hero/core/datasource/http/api_constans.dart';
import 'package:code_hero/core/datasource/http/interceptors/auth_interceptor.dart';
import 'package:code_hero/core/datasource/http/interceptors/data_extractor_interceptor.dart';
import 'package:code_hero/core/logger/logger.dart';
import 'package:dio/dio.dart';

import '../interceptors/error_interceptor.dart';
import '../interceptors/info_interceptor.dart';

Dio getDio() {
  final options = BaseOptions(
    baseUrl: apiBaseUrl,
    connectTimeout: const Duration(minutes: 2),
    receiveTimeout: const Duration(minutes: 2),
  );
  final dio = Dio(options);

  dio.interceptors
    ..add(AuthInterceptor())
    ..add(ErrorInterceptor())
    ..add(InfoInterceptor(log: LoggerImpl()))
    ..add(DataExtractorInterceptor());
  return dio;
}
