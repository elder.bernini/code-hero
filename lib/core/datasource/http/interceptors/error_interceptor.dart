import 'dart:io';

import 'package:code_hero/core/helpers/failure/failure.dart';
import 'package:dio/dio.dart';

class ErrorInterceptor extends Interceptor {
  @override
  Future onError(DioException err, ErrorInterceptorHandler handler) async {
    DioException formatedError = _validateError(err);

    handler.next(formatedError);
  }

  DioException _validateError(DioException err) {
    if (err.error is SocketException) {
      return _getDioError(err, WithoutConnectionFailure());
    }

    if (err.type == DioExceptionType.connectionTimeout ||
        err.type == DioExceptionType.sendTimeout ||
        err.type == DioExceptionType.receiveTimeout) {
      return _getDioError(err, TimeoutFailure());
    }

    switch (err.response?.statusCode) {
      case 401:
      case 403:
        return _getDioError(
          err,
          UnnauthorizedFailure(
            message: _getMessage(err),
          ),
        );

      case 400:
        return _getDioError(
          err,
          BadRequestFailure(
            message: _getMessage(err),
          ),
        );

      case 404:
        return _getDioError(
          err,
          NotFoundRequestFailure(),
        );

      case 429:
        return _getDioError(
          err,
          UnnauthorizedFailure(
            message: _getMessage(err),
          ),
        );

      case 500:
      default:
        return _getDioError(
          err,
          DatasourceFailure(
            message:
                '${DatasourceFailure.mensagemErroGenerica} Falha interna no servidor',
          ),
        );
    }
  }

  DioException _getDioError(DioException err, DatasourceFailure failure) {
    return DioException(
      requestOptions: err.requestOptions,
      error: failure,
      response: err.response,
      type: err.type,
    );
  }

  String? _getMessage(DioException error) {
    if (error.response == null) return null;
    if (error.response?.data is Map) {
      return error.response?.data['message'];
    }
    return null;
  }
}
