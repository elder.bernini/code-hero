import 'package:dio/dio.dart';

class DataExtractorInterceptor extends Interceptor {
  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    if (response.statusCode == 200 && response.data is Map) {
      var res = Response(
        requestOptions: response.requestOptions,
        data: response.data['data'],
      );
      handler.next(res);
    } else {
      handler.next(response);
    }
  }
}
