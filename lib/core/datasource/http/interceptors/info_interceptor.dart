import 'dart:convert';

import 'package:code_hero/core/logger/logger.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class InfoInterceptor extends Interceptor {
  final Logger _log;

  InfoInterceptor({
    required Logger log,
  }) : _log = log;

  @override
  Future<void> onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    if (!kReleaseMode) {
      _log.append('##### >> Request LOG >> #####');
      _log.append('>> ${options.method.toUpperCase()} - ${options.uri}');
      _log.append('headers: ${options.headers}');
      _logData(
        options.headers[Headers.contentTypeHeader],
        options.data,
      );
      _log.append('##### >> Request LOG >> #####');
      _log.closeAppend();
    }

    handler.next(options);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    if (!kReleaseMode) {
      _log.append('##### << Response LOG << #####');
      _log.append(
          '<< ${response.statusCode}:${response.requestOptions.method} - ${response.requestOptions.uri}');
      _log.append('data: ${json.encode(response.data)}');
      _logData(
        response.headers.value(Headers.contentTypeHeader),
        response.data,
      );
      _log.append('##### << Response LOG << #####');
      _log.closeAppend();
    }

    handler.next(response);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) async {
    if (!kReleaseMode) {
      _log.append('##### << Error LOG << #####');
      _log.append(
          '<< ${err.requestOptions.method} - ${err.requestOptions.uri}');
      _log.append('code: ${err.response?.statusCode}');
      _log.append('response: ${err.response}');
      _log.append('##### << Error LOG << #####');
      _log.closeAppend();
    }

    handler.next(err);
  }

  void _logData(String? contentType, dynamic data) {
    if (data == null) return;

    if (contentType?.contains(Headers.jsonContentType) == true) {
      _log.append('data: ${json.encode(data)}');
    }

    if (data is FormData) {
      final value = data.fields
          .map((e) => '[${e.key}:${e.value}] ')
          .reduce((value, element) => value += element);

      _log.append('data: $value');
    }
  }
}
