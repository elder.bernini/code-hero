import 'dart:convert';

import 'package:code_hero/core/datasource/http/api_constans.dart';
import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';

class AuthInterceptor extends Interceptor {
  @override
  Future<void> onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    options.queryParameters;

    int time = DateTime.now().millisecondsSinceEpoch;
    String hash = md5
        .convert(utf8.encode(time.toString() + apiPrivateKey + apiPublickKey))
        .toString();

    options.queryParameters.addAll(
      {
        'ts': time.toString(),
        'apikey': apiPublickKey,
        'hash': hash,
      },
    );

    handler.next(options);
  }
}
