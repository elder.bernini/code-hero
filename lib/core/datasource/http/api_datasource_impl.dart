import 'package:code_hero/core/datasource/http/api_datasource.dart';
import 'package:code_hero/core/datasource/http/dio/safe_dio_call.dart';
import 'package:code_hero/core/helpers/failure/failure.dart';
import 'package:code_hero/core/helpers/type_defs/function_type_defs.dart';
import 'package:code_hero/core/logger/logger.dart';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

class ApiDatasourceImpl extends ApiDatasource {
  final Dio _dioClient;
  final Logger _logger;

  ApiDatasourceImpl({
    required Dio dioClient,
    required Logger logger,
  })  : _dioClient = dioClient,
        _logger = logger;

  @override
  Future<Either<Failure, T>> get<T>(
    String path,
    FromJsonFactory<T> responseFactory, {
    Map<String, dynamic>? params,
  }) {
    return _safeDioCall(
      GetSafeDioCall(
        dio: _dioClient,
        path: path,
        params: params,
      ),
      responseFactory,
    );
  }

  Future<Either<Failure, T>> _safeDioCall<T>(
    SafeDioCall safeDioCall,
    FromJsonFactory<T> responseConverter,
  ) async {
    try {
      final res = await safeDioCall.execute();

      final jsonResponse = res.data;

      if (jsonResponse == null || jsonResponse == '') {
        return right(null as T);
      }

      T apiResponse = responseConverter(jsonResponse);

      return right(apiResponse);
    } on TypeError catch (te, st) {
      _logger.error(te.toString(), te, st);

      return left(DatasourceFailure(
          message:
              '${DatasourceFailure.mensagemErroGenerica} Falha ao converter retorno do servidor'));
    } on DioException catch (de) {
      if (de.error is DatasourceFailure) {
        return left(de.error as DatasourceFailure);
      }

      return left(DatasourceFailure(
          message:
              '${DatasourceFailure.mensagemErroGenerica} Erro inesperado de comunicação com o servidor'));
    } catch (er, st) {
      _logger.error(er, er, st);

      return left(DatasourceFailure(
          message:
              '${DatasourceFailure.mensagemErroGenerica} Falha inesperada de comunicacao com o servidor'));
    }
  }
}
