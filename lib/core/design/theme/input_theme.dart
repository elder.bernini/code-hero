import 'package:code_hero/core/helpers/dimensions.dart';
import 'package:flutter/material.dart';

final inputDecorationTheme = InputDecorationTheme(
  border: OutlineInputBorder(
    borderRadius: BorderRadius.circular(inputBorderRadius),
    borderSide: const BorderSide(
      color: Colors.grey,
      width: 1,
    ),
  ),
);
