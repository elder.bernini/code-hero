import 'package:code_hero/core/design/colors/app_colors.dart';
import 'package:flutter/material.dart';

Future<T?> showLoadingAppDialog<T>({
  required BuildContext context,
  bool isDismissible = false,
  String message = 'Carregando',
}) {
  return _show(
    context: context,
    isDismissible: isDismissible,
    content: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(message),
        const SizedBox(height: 16),
        LinearProgressIndicator(
          color: primary.withAlpha(100),
        ),
      ],
    ),
  );
}

Future<T?> _show<T>({
  required BuildContext context,
  required bool isDismissible,
  required Widget content,
  Widget? title,
  List<Widget>? actions,
}) {
  return showAdaptiveDialog(
    context: context,
    barrierDismissible: isDismissible,
    builder: (_) {
      return PopScope(
        canPop: isDismissible,
        child: AlertDialog(
          title: title,
          actions: actions,
          backgroundColor: Colors.white,
          content: content,
        ),
      );
    },
  );
}

void dismissAppDialog(BuildContext context) {
  if (ModalRoute.of(context)?.isCurrent != true) {
    Navigator.of(context).pop();
  }
}
