import 'package:code_hero/core/design/colors/app_colors.dart';
import 'package:flutter/widgets.dart';

const titleBlack = TextStyle(
  fontSize: 16,
  color: primary,
  fontWeight: FontWeight.w900,
  height: 1.2,
);
const titleLight = TextStyle(
  fontSize: 16,
  color: primary,
  fontWeight: FontWeight.w300,
  height: 1.2,
);
const pRegular = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.normal,
  height: 1.2,
);
const p2Regular = TextStyle(
  fontSize: 21,
  color: font,
  fontWeight: FontWeight.normal,
  height: 1.1,
);
