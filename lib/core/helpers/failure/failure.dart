class Failure {
  String message;

  Failure({
    required this.message,
  });

  @override
  String toString() {
    return 'Failure: $message';
  }
}

class DatasourceFailure extends Failure {
  static const String mensagemErroGenerica =
      'Não foi possível completar a requisição.';

  DatasourceFailure({
    String? message,
  }) : super(message: message ?? '$mensagemErroGenerica. Falha inesperada');
}

class WithoutConnectionFailure extends DatasourceFailure {
  WithoutConnectionFailure()
      : super(
          message:
              '${DatasourceFailure.mensagemErroGenerica}. Verifique a conexão e tente novamente',
        );
}

class UnnauthorizedFailure extends DatasourceFailure {
  UnnauthorizedFailure({String? message})
      : super(
          message: message ??
              '${DatasourceFailure.mensagemErroGenerica} Não autorizado',
        );
}

class BadRequestFailure extends DatasourceFailure {
  BadRequestFailure({String? message})
      : super(
          message: message ??
              '${DatasourceFailure.mensagemErroGenerica} Request inválido',
        );
}

class NotFoundRequestFailure extends DatasourceFailure {
  NotFoundRequestFailure()
      : super(
          message:
              '${DatasourceFailure.mensagemErroGenerica} Recurso não encontrado',
        );
}

class TooManyRequestsFailure extends DatasourceFailure {
  TooManyRequestsFailure({String? message})
      : super(
          message: message ??
              '${DatasourceFailure.mensagemErroGenerica} Não autorizado',
        );
}

class TimeoutFailure extends DatasourceFailure {
  TimeoutFailure()
      : super(
          message:
              '${DatasourceFailure.mensagemErroGenerica} Tempo limite excedido',
        );
}
