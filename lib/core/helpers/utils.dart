import 'package:code_hero/core/design/components/app_dialog.dart';
import 'package:code_hero/core/helpers/states/state_result.dart';
import 'package:flutter/widgets.dart';

void hideKeyboard() => FocusManager.instance.primaryFocus?.unfocus();

void checkLoading(
  BuildContext context,
  StateResult state,
) {
  if (state is LoadingStateResult) {
    showLoadingAppDialog(context: context);
  }

  if (state is HideLoadingStateResult) {
    dismissAppDialog(context);
  }
}
