import '../failure/failure.dart';

abstract class StateResult {}

class LoadingStateResult extends StateResult {}

class HideLoadingStateResult extends StateResult {}

class DefaultStateResult extends StateResult {}

class EmptyStateResult<T> extends StateResult {
  final T? data;
  EmptyStateResult({this.data});
}

class FailureStateResult extends StateResult {
  final Failure failure;
  FailureStateResult(this.failure);
}

class SuccessStateResult<T> extends StateResult {
  final T data;
  SuccessStateResult(this.data);
}
