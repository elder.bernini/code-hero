<h1 align="center">
  Code Hero
</h1>

- [Introdução](#introdução)
- [Instalação](#instalação)
- [Configuração](#configuração)
- [Build and Test](#build-and-test)

# Introdução

Aplicativo de teste que realiza consulta aos herois da Marvel utilizando a api disponibilizada pela mesma

## Instalação

1. [Instalar o Flutter](https://flutter.dev/docs/get-started/install) <br/>
2. [Instalar o VS Code](https://code.visualstudio.com/)<br/>
   2.2 add plugin [Flutter_MobX](https://marketplace.visualstudio.com/items?itemName=Flutterando.flutter-mobx) (opcional)<br/>
3. Clone este repositório e instale as dependências

```bash
git clone https://gitlab.com/elder.bernini/code-hero.git
```

4. Abrir a pasta raiz do projeto
5. Para instalar as dependencias, executar os comandos abaixo abaixo na pasta raiz do projeto

```
flutter clean
flutter pub get
flutter pub run build_runner build --delete-conflicting-outputs
```

# API Key

Para acesso da api Marvel, é necessário obter a developer key disponibilizada pela empresa. Realizar o cadastro e obtenção da Key em [https://developer.marvel.com]<br/>
Apos a obtenção das keys, incluir as mesmas no arquivo `lib/core/datasource/http/api_constants.dart`


# Build and Run

[Executando e debugando](https://flutter.dev/docs/development/tools/vs-code#running-and-debugging) <br/>

Para executar localmente utilizar os parametros já configurados no vscode no menu 'Run and Debug'
<br/>
Caso preferir executar manualmente utilizar os comandos abaixo
```bash
## Run
flutter run 

## Build
flutter build

```